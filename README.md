# Team B3

Team B3 ITT2 datacenter project 

# Team Members

- Aleksandra Voronina
- Aubrey Jones
- Dainty Olsen
- Gladys Waithera
- Henrik Hansen
- Lukasz Zwak
- Thobias Selmann


# Important Links
- [B3 Website](https://21s-itt2-datacenter-students-group.gitlab.io/team-b3/)
- [Project Plan](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/project_plan_for_students.md)
- [Pre-Mortem](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Pre%20Mortem)
- [Brainstorm 1](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/Brainstorm_Results.md)
- [Brainstorm 2](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/Brainstorm_Part%202.md)
- [Block Diagram of System](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Images%20files/Data_AI_-_Diagram.png)
- [Requirements Draft](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/Requirements_draft.md)
- [Use Case](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/Use_Case.md)
- [MQTT Publisher](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/publisher.py)
- [MQTT Subscriber](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/Subscriber.py)
- [OME Research](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/research_OME.md)
- [Measuring Techniques](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/SensorNotes.md)
- [Sensor Requirements](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/Sensor%20Requirements.md)
- [Sensor Shopping List](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/Sensor%20Shopping%20List.md)
- [Scrum Schedule](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/Scrum%20Schedule.md)
- [Block Diagram of PoC](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Images%20files/Data_AI_-_Diagram.png)
- [Milestones](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/milestones)
- [Data Persistence via Mongo-DB](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/Mongo_Data_Persistence.md)
- [PoC Progress Video](https://youtu.be/xtWjgl0ePpc)
- [MQTT Security Notes](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/MQTT_Security.MD)
- [Business Research](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/Business_Understanding.md)
- [Business Org Chart](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/org_chart.png)
- [MVP Presiontation Video](https://drive.google.com/file/d/1ZtOe7-wyqVWM2-XfMJDzjBFJESdnfXqL/view?usp=sharing)

# B3 Link Depository
- [MQTT Database](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3-group/mqtt-to-database/-/blob/master/README.md)
- [MQTT Publisher](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3-group/mqtt-publisher/-/blob/master/README.md)
- [MQTT Subscriber](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3-group/mqtt-subscriber/-/blob/master/README.md)
- [Node Red](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3-group/node-red/-/blob/master/README.md)
- [Proof of Concept](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3-group/project-proof-of-concept/-/blob/master/README.md)
- [Minimum Viable Product](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3-group/project-minimum-viable-project/-/blob/master/README.md)

