import RPi.GPIO as GPIO
import time
import math
from ADCDevice import *
import paho.mqtt.client as mqtt

#Broker information
brokerIP = ""
brokerPort = 1883
brokerKeepAlive = 60

#MQTT Settings
myTopic = ""
myQos = 1
myRetain = True

def mqtt_connect():
    client = mqtt_Client()
    client.connect(brokerIP,brokerPort,brokerKeepAlive)

def mqtt_publish(data):
    client = mqtt_Client()
    client.publish(topic=myTopic,qos=myQos,payload=data,retain=myRetain)
    print(str(data) + " has been published on broker " + brokerIP + ":" + str(brokerPort) + " on topic " + myTopic)

def mqtt_disconnect():
    client = mqtt_Client()
    client.disconnect()

adc = ADCDevice() # Define an ADCDevice class object

def setup():
    global adc
    if (adc.detectI2C(0x48)): # detect the pcf8591
        adc = PCF8591()
    elif(adc.detectI2C(0x4b)): # detect the ads 7830
        adc = ADS7830()
    else:
        print(
        "No correct I2C address found, \n"
        "Please use command 'i2cdetect -y 1' to check the I2c adress! \n"
        "Program Exit. \n"
        )
        exit(-1)

def loop():
    while True:
        value = adc.analogRead(0)   # read ADC value A0 pin
        voltage = value / 255.0 * 3.3
        Rt = 10 * voltage / (3.3 - voltage) # calculate resistance value of themistor
        tempK = 1/(1/(273.15 + 25) + math.log(Rt/10)/3950.0) # calculate temperature (Kalvin)
        tempC = tempK -273.15 # calculate temperature (Celsius)

        print("ADC Value : %d, Voltage : %.2f, Temperature : %.2f" %(value, voltage,tempC))
        mqtt_publish(tempC)
        
        time.sleep(5)

def destroy():
    mqtt_disconnect()
    adc.close()
    GPIO.cleanup()

if __name__ == "__main__":
    print("Program is starting...")
    setup()
    try:
        mqtt_connect()
        loop()
    except KeyboardInterrupt: # Press ctrl-c to end the program.
        destroy()
