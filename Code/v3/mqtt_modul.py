import paho.mqtt.client as mqtt
#Paho settings
client = mqtt.Client()

class mqttsetup:

    def Connect(self,brokerIP,brokerPort,brokerKeepAlive):
        """
        This Function takes 3 values, brokerIP, brokerPort and brokerKeepAlive. 
        
        brokerIP is the ip addresse of the mqtt broker, Eksample 100.1.1.1 or "test.mqtt.com".
        brokerPort is the port of the mqtt broker, Eksample 8080.
        brokerKeepAlive is how long the connection will be keep open with the mqtt broker, Eksample 60.
        
        Function eksample.
        Connect("test.mqtt.com",8080,30)
        """
        
        self.brokerIP = brokerIP
        self.brokerPort = brokerPort
        self.brokerKeepAlive = brokerKeepAlive

        client.connect(brokerIP,brokerPort,brokerKeepAlive)
        
        print("Connecting to broker.")


    def Publish(self,myPayload,myTopic,myQos,myRetain):
        """
        This Function takes 4 values, myPayload, myTopic, myQos and myRetain.
        
        myPayload is the data that need to be published to the mqtt broker, Could be a str, int or float.
        myTopic is the topic for which the payload will be broadcasted to, Eksample test/channel.
        myQos is the Qos setting for mqtt, takes 3 values, 0,1 and 2.
        myRetain is wether the message should be retained to show new subscribers, takes 2 values, True or Falsh.
        
        Function eksample.
        Publish(42,"test/channel",1,True)
        """

        client.publish(topic=myTopic,qos=myQos,payload=myPayload,retain=myRetain)

        print(str(myPayload) + " has been published on broker " + self.brokerIP + ":" + str(self.brokerPort) + " on topic " + myTopic)

    def Disconnect(self):
        """
        This function ends the mqtt connection, it takes no values.
        """

        client.disconnect()

        print("Disconnecting from broker.")
