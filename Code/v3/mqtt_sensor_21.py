import RPi.GPIO as GPIO
import time
import Freenove_DHT as DHT
from mqtt_modul import mqttsetup
DHTPin = 11
mq = mqttsetup()

def loop():
    dht = DHT.DHT(DHTPin)
    sumCnt = 0
    while(True):
        sumCnt += 1
        chk = dht.readDHT11()

        print("The sumCnt is : %d, \t chk    : %d"%(sumCnt,chk))
        if(chk is dht.DHTLIB_OK):
            print("DHT11,OK!")
        elif(chk is dht.DHTLIB_ERROR_CHECKSUM):
            print("DHTLIB_ERROR_CHECKSUM!!")
        elif(chk is dht.DHTLIB_ERROR_TIMEOUT):
            print("DHTLIB_ERROR_TIMEOUT!")
        else:
            print("Other error!")

        print("Humidity : %.2f, \t Temperature : %.2f \n"%(dht.humidity,dht.temperature))

        mq.Connect("duckmqttduck.northeurope.cloudapp.azure.com",1883,30)
        mq.Publish(dht.humidity,"rpi/humi",1,True)
        mq.Publish(dht.temperature,"rpi/temp",1,True)
        mq.Disconnect()

        time.sleep(2)

if __name__ == '__main__':
    print('Program is starting ... ')
    try:
        loop()
    except KeyboardInterrupt:
        GPIO.cleanup()
        exit()

