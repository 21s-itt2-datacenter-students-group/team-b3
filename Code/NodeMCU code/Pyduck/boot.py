# This file is executed on every boot (including wake-boot from deepsleep)
import time
from umqttsimple import MQTTClient
import ubinascii
import micropython
import network
import esp
esp.osdebug(None)
import gc
gc.collect()

ssid = 'ssid of wifi'
password = 'password'

station = network.WLAN(network.STA_IF)

station.active(True)
station.connect(ssid, password)

while station.isconnected() == False:
  print("Wifi connection lost!")
  time.sleep(2)

print('Connection successful')
print(station.ifconfig())

