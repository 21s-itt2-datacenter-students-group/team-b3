import RPi.GPIO as GPIO
import time
import Freenove_DHT as DHT
import paho.mqtt.client as mqtt
DHTPin = 11

#Broker information
brokerIP = "duckmqttduck.northeurope.cloudapp.azure.com"
brokerPort = 1883
brokerKeepAlive = 300

#MQTT Settings
myTopic = "rpi/humi"
myTopic2= "rpi/temp"
myQos = 1
myRetain = True 
client = mqtt.Client()

def mqtt_connect():
    #client = mqtt.Client()
    client.connect(brokerIP,brokerPort,brokerKeepAlive)

def mqtt_publish(data,data2):
   
    client.publish(topic=myTopic,qos=myQos,payload=data,retain=myRetain)
    client.publish(topic=myTopic2,qos=myQos,payload=data2,retain=myRetain)
    print(str(data) + " has been published on broker " + brokerIP + ":" + str(brokerPort) + " on topic " + myTopic)
    
    print(str(data2) + " has been published on broker " + brokerIP + ":" + str(brokerPort) + " on topic " + myTopic2)

    
def mqtt_disconnect():
    #client = mqtt.Client()
    client.disconnect()


def loop():
    dht = DHT.DHT(DHTPin)
    sumCnt = 0
    while(True):
        sumCnt += 1
        chk = dht.readDHT11()

        print("The sumCnt is : %d, \t chk    : %d"%(sumCnt,chk))
        if(chk is dht.DHTLIB_OK):
            print("DHT11,OK!")
        elif(chk is dht.DHTLIB_ERROR_CHECKSUM):
            print("DHTLIB_ERROR_CHECKSUM!!")
        elif(chk is dht.DHTLIB_ERROR_TIMEOUT):
            print("DHTLIB_ERROR_TIMEOUT!")
        else:
            print("Other error!")

        print("Humidity : %.2f, \t Temperature : %.2f \n"%(dht.humidity,dht.temperature))

        mqtt_publish(dht.humidity,dht.temperature)

        time.sleep(2)

if __name__ == '__main__':
    print('Program is starting ... ')
    try:
        mqtt_connect()
        loop()
    except KeyboardInterrupt:
        GPIO.cleanup()
        mqtt_disconnect()
        exit()

