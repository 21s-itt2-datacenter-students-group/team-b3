#Broker information
brokerIP = ""
brokerPort = 1883
brokerKeepAlive = 60

#MQTT Settings
myTopic = ""
myQos = 1
myRetain = True

def mqtt_connect():
    client = mqtt_Client()
    client.connect(brokerIP,brokerPort,brokerKeepAlive)

def mqtt_publish(data):
    client = mqtt_Client()
    client.publish(topic=myTopic,qos=myQos,payload=data,retain=myRetain)
    print(str(data) + " has been published on broker " + brokerIP + ":" + str(brokerPort) + " on topic " + myTopic)

def mqtt_disconnect():
    client = mqtt_Client()
    client.disconnect()
