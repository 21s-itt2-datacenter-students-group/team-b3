2/10/21 SCRUM 

SCRUMLORD - Thobias
Writelord - Aubrey
ATTENDEES - Aleksandra, Dainty, Henrik, Lukasz
NEXT SCRUMLORD - Henrik Holm Hansen

P1 - Talk about Assignment according to deadlines

	We closed a couple of issues on the Gitlab Board, and decided on what we would be working on this week. 
	
	
P2 - Rest of the teams concerns

	Aleksandra - No Project managers isn't helpful for project cohesion, we need to know our budget if we're going to work on things.
	Aubrey - Annoyed about no direction, wants literally any guidance
	Dainty - Unsure about what sensors we're supposed to be using, but hopeful that things will improve
	Henrik - NO COFFEE (but his concerns were voiced by the other team members)
	Lukasz - No Concerns
	Thobias - Annoyed about lack of guidance (OMEs can't direct us/don't know about what our project is supposed to do), Has plans for the next week.
	
P3 - Excercises for WW06

E1 - System Brainstorm 
	Do a Brainstorm & link to readme
	Anaylyze the System
	Draft up a list of Requirements
E2 - MQTT Intro
	Read Instructions
	Install Paho MQTT
	Create a Publisher
	Creat a Subscriber
	Do a Local Test
	Test betwixt Team Members
E3 - Research OME 
	Research the OME education using a list of questions provided\
	
P4 - Round About

WE NEED TO MEET UP AFTER CLASS AND START COMPARING NOTES AND SUCH
