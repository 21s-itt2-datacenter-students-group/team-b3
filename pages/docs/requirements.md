## System Brainstorm
At the beginning of the project we brainstormed about the project and issues that might arise and how me might fix those issues. We documented what we came up with over two seperate sessions, and the lists can be found [here](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/Brainstorm_Part%202.md) and [there](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/edit/master/Related%20Files/Brainstorm_Results.md).
  
## Use Cases
This project is an IoT Device built to monitor the humidity and temperature of server racks within a Data Center. The Device will monitor these values over the course of 15 minutes, recording the data in smaller increments of time and eventually averaging them out to get a more accurate value. This value is then sent via WiFi to an easy to read dashboard to help the technicians in the Data Center keep tabs on how the servers are doing and if they need maintenance or repairs.  

## Requirements Draft  

* DHT11 (Temperature and Humidity Sensor)
* TE Connectivity HPP801 (Temperature Sensor)
* Veroboard
* Jumper Wires
* Resistors
* Power Supply
* 9V Battery Connector
* NodeMCU
