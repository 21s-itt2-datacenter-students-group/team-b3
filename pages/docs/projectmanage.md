We used Gitlab to organize what we needed to accomplish using the issue board to cut the large tasks that we were assigned into smaller, more manageable chuncks. To prepare for work and to get everyone on the same page we held a scrum every Monday and Wednesday (Our project days) and a biweekly Sprint to make sure that we were kept on track. We admittedly didn't work with the OMEs as much as we should have.

We organized our Gitlab project into a couple of manageable and descriptive folders where we would store the various files; for example a folder to store notes from our daily scrum and a folder to store necessary code. Later in the semester we started to reorganize, making new gitlab projects for smaller pieces so that the main page wouldn't be so clogged with scripts and notes.

In the beginning, we didn't work with either but as the semester continued we began to see the benefits of pulling, modifying, and pushing a file without the gitlab browser. One thing on Gitlab that we didn't end up getting to test out too much was the milestone system, but in future projects we will endeavor to figure those out to better our understanding of Gitlab and how it works.

At the start of this project we were given last year's plan as an example of what to strive towards. We got together as a group and had a meeting about it, trying to work through how our project would look within the framework provided. The final result is up on our [Gitlab](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/project_plan_for_students.md) if you'd like to take a look.

In the first week of the project we prepared for this project by brainstorming what might go wrong during the course of this semester's project which is a common thing to do in project development. We came up with a large list of potential threats to our project and slowly whittled down the list until it was a solid 10. The next step in the process was to find solutions to the problems we had come up with so that we could minimize problems in production. The list (and solutions) that we came up with are on our Gitlab and you can [check](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Pre%20Mortem) them out if you're interested.



***A Little About the OME's***
	   
	   
**What is the learning goals of the education?**  
To attain knowledge and skills needed to be responsible for management
and maintenance of technical facilities and installations both on- and offshore.

**What educational level in the qualification framework is the OME education?**  
The OME education is placed at educational level 6 out of 8 total levels.
This means that students attain knowledge of theory, methodologies
and practice within a given field, as well as being able to understand and reflect on these.

**What courses are included in the education?**  
The education includes courses in Methodology, thermic machinery
and installations, electrical and electronic machinery, installations
and equipment, automation as well as management, finance and security.

**What kind of jobs does OME's occupy?**  
OMEs are hired for Project Management, Sales Engineering, Environment-
and Quality control manager, technical manager, advisory positions regarding energy.

**What challenges can we experience in our communication with OME's**  
Having different understanding or angles of various elements in a project, realizing when such situations occur and how to get a common understanding where all members can agree on a relevant solution within narrow time constraints.
