# Welcome to Team B3's Project Website

For full documentation visit [Our Gitlab](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/README.md).

## Overview

This project will innovate how the Datacenter Industry monitors the heat and humidity that comes off of data racks. Our solution will be more affordable than other leading solutions and more reliable. 

The purpose of our project was to both get an idea as to how we as students could use our skills within the industry and to take a hands on approach towards that goal. Our project is a temperature and humidity sensor and its intent is to efficiently capture heat and humidity data from the server racks and send that data to a database that displays the heat fluctuations in an easy to understand format.

![System Overview](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/raw/master/Images%20files/Data_AI_-_Diagram.png "System Overview")

## Node Red dashboard  
Here is a preview of how our dashboard will look while it is operating. Green means that all is well, and red means that there is an issue that needs to be addressed. If you need to check on a rack, you can click that individual rack's label and you will be led to that unit's dashboard so you can get a better idea as to what might be going wrong.  
  
  
![Node Red](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/raw/master/Images%20files/dashboard.jpg)
  

  
![Node Red](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/raw/master/Images%20files/dashboard2.jpg)
