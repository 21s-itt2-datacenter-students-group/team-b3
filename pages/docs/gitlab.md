Our Gitlab Repository is [here](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3). It is messy but you should be able to find all the information you need by looking through the readme at the bottom of the page.

You can find the second semester project's course material by following this [link](https://eal-itt.gitlab.io/21s-itt2-project/). You can gleam a bit of the learning process by looking through the weekly plans and daily excercises to see what we were doing while we worked on this project.

You can find the official documentation for our project [here](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/tree/master/Related%20Files). There you can find the information necessary to understand the project and how it works.
