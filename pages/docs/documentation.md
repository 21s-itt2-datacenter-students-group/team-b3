**Project Plan**

# Background

This is the semester ITT2 project where you will work with different projects, initated by a company. This project plan will cover the project, and will match topics that are taught in parallel classes.

The overall project case is described at 
[https://datacenter2.gitlab.io/datacenter-web/](https://datacenter2.gitlab.io/datacenter-web/)

# Purpose

The main goal is to have a system where IoT sensors collect data from a datacenter.
This is a leaning project and the primary objective is for the students to get a good understanding of the challenges involved in making such a system and to give them hands-on experience with the implementation.

For simplicity, the goals stated wil evolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.

# Goals

The overall system that is going to be build looks as follows:  
![project_overview](https://eal-itt.gitlab.io/datacenter-iot/assets/images/index/datacenter_iot_system_overview.png "ITT2 project overview")  
![project_overview](https://datacenter2.gitlab.io/datacenter-web/images/project_overview_datacenter2.png "ITT2 project overview")

Reading from the left to the right:  

* Sensor modules 1-3: A placeholder for x number of sensors
* Raspberry Pi: The embedded system to run the sensor software and to be the interface to the MQTT broker.  
* Computer: Connects to both the Raspberry Pi and Gitlab while developing and troubleshooting  
* MQTT broker: MQTT allows for messaging between device to cloud and cloud to device. This makes for easy broadcasting messages to groups of things. 
* Consumer: This will be the Data Center and/or the Mechanical Engineering students

Project deliveries are:  

* A system reading and writing data to and from the sensors/actuators  
* Docmentation of all parts of the solution  
* Regular meeting with project stakeholders.  
* Final evaluation 

# Schedule

See the [lecture plan](https://eal-itt.gitlab.io/21s-itt2-project/other-docs/21S_ITT2_PROJECT_lecture_plan.html) for details.

# Organization

The Steering Committee is Nikolaj Simonsen - nisi@ucl.dk and Mathias Gregersen - megr@ucl.dk

The Project Managers for this project are the Operational Maintenance Engineers (OME).

The project group with competencies in IT Technologies consists of, Aleksandra Voronina, Aubrey Jones, Dainty Olsen, Gladys Waithera, Henrik Holm Hansen, Lukasz Zwak and Thobias Selmann.

The external resource groups are the members of our class, our teachers, and the Machine Engineer students that we're working with.


# Budget and resources

Small monetary resources are expected. In terms of manpower, only the people in the project group are expected to contrbute.


# Risk assessment

A link to our pre-mortem meeting (and the most likely risks involved) can be found [here](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Pre%20Mortem)
# Stakeholders

Possible stakeholders 

Mechanical Engineering Students (External, Active, Positive)-(TBD)

Internal vs. external  
Positive vs. negative  
Active vs. passive  

A strategy could be planned on how to handle each stakeholder and how to handle stakeholders different (and/or conflicting) interests and priorities.  
This could also include actions designed to transform a person or a group into a (positive) stakeholder, or increase the value of the project for a given stakeholder.

# Communication

We will hold a weekly meeting with our stakeholder, preplanned by our steering committee and hopefully we will have the chance to meet them physically if the covid situation improves. Logs of our communication will be stored here. Our group will communicate with eachother via Discord, Facebook groups and Emails as well as the mentioned weekly meetings.


# Perspectives

[Project is not yet complete]

# Evaluation

[Project is not yet complete]

# References
The project is managed using gitlab.com. Our group's work can be found [here](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3)
  ---
**Pre Mortem**  
Earlier in the semester we completed a Pre-Mortem, which is a brainstorm where we thought up possible ways the project could fail, in the hopes that we could plan for it. The following list is what we came up with at the time, as well as what we came up with to mitigate the problem.  
**Corona**  
	Corona is a global pandemic, so it might mess with our project plans. We don't really don't have a fix for this one, but neither does anyone else.

**Poor Team Management & Lack of Team Member Engagement**  
	We had too much fun working together during the first semester and our teamwork and engagement. We therefore are going to take a few steps back and attempt to work as colleagues instead of friends. We will attempt to divert our social aspects after working hours instead of during working hours.

**Misanagement of Time**  
	We will be using the SCRUM method of project and time management to make better use of our time.

**Unrealistic Goals**  
	We will be more realistic with ourselves, and we will try to recognize when we are adding unnecessary or complicated goals.

**Distractions (Home and School)**  
	We will change up how we pair up to work, and we are going to be better about squashing small talk and other distractions.

**Missing or Broken Components**  

**Random Accidents and Sickness**  

**Stuck at Home**  
	We will start having Cameras on when we work together to simulate spending time away from home.

**Can't Meet Up**  
	We will start having Cameras on when we work together to simulate spending time away from home.
    
**Small Group**  
	We will be more realistic with ourselves, and we will try to recognize when we are adding unnecessary or complicated goals.  

---
# Proof of Concept and Minimum Viable Product
A link to a video showing the progress we have made on our Proof of Concept can be found [here](https://www.youtube.com/watch?v=xtWjgl0ePpc&feature=youtu.be) and a link to a video showing off our Minimum Viable Product can be found [here](https://drive.google.com/file/d/1ZtOe7-wyqVWM2-XfMJDzjBFJESdnfXqL/view)
