##Measuring Techniques

While researching which sensors we wanted to use in this project we documented quite a bit on how the different types of sensors we might want to try using worked, as well as their pros and cons. The list can be seen [here](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/SensorNotes.md), if you're interested in how the sensors work.

##Sensor Requirements
After looking into the different types of sensors that exist, we needed to draft up what we required from the sensors. This list is what we came up with, from most important to least important.

1. The environment is within the operational limits of the sensor (22-35 C) and (30-60% humidity)
2. The price is relatively low
3. The accuracy of the sensor is relatively high
4. The sensor is reliable
5. The part is not rare or hard to acquire
6. The energy consumption and heat generation of the sensor are not too high
7. It needs to be somewhat small
  
##The Sensors
After much deliberation, we decided on which sensors would go into our product. The following chart will provide links, prices and any ntoes that we came up with regarding how the devices operate.

|  Measuring Technique  |  Sensor Name  |  Link to Supplier  |  Cost  |  Pros  |  Cons  |
| --- | --- | --- | --- | --- | --- |
| Temperature | TE Connectivity HPP801A031 | https://dk.rs-online.com/web/p/temperatursensor-og-fugtighedssensor-ic-er/8937082//?cm_mmc=aff2-_-dk-_-octopart-_-Instock8937082 |  60.52 dkk  |  Don't need to calibrate it to switch it out, Lead Free! | Kinda Pricey TBH |
|  Temp and Humidity  |  Seeed Studio Sensorudviklingssæt  |  https://dk.rs-online.com/web/p/sensor-udvikling/1743237/  |  49 dkk  |  It can compare temperature ***AND*** Humidity  |  It's already attached to a board ;_;  |
|  Humidity Sensor  |  HCZ-D5-A  |  https://dk.farnell.com/multicomp/hcz-d5-a/sensor-humidity-20-90-rh-5/dp/1891428  |  42,22 dkk  |  It covers the specifications that we need  |    |  
|  NONE  |  9v battery connector  |  https://dk.rs-online.com/web/p/batterikontakter/1854775/?cm_mmc=DK-PLA-DS3A-_-google-_-CSS_DK_DK_Batterier_og_opladere_Whoop-_-(DK:Whoop!)+Batterikontakter-_-1854775&matchtype=&pla-329509881343&gclid=Cj0KCQiAvvKBBhCXARIsACTePW8xjQNx2FKXu8LRzGhy5OnJBZNEfZciBEH01UmKfjvGuELeVEqD3UwaAoHREALw_wcB&gclsrc=aw.ds  |  6,09dkk  |  NONE  |  NONE  |
|  NONE  |  Stripboard  |  https://dk.rs-online.com/web/p/laboratorieprint-med-laengdestriber/0434627/  |  175,94dkk  |  NONE  |  NONE  |

Alternatively, if that chart doesn't load correctly for some reason you can find a link to the pieces we chose on our [Gitlab](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Related%20Files/Sensor%20Shopping%20List.md)
