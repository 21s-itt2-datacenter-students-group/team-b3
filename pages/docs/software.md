## MQTT
We are using MQTT (Message Queuing Telemetry Transport) is the protocol that we are using to send data to and from our IoT Device. MQTT works through a publish and subscribe protocol, wherein a device can send data to a broker (explanation of a broker) and another device will subscribe to that same broker to read the data sent. (More Data to come)  

The sensors that we are going to be using require that we can recieve the data that they are sending to the Raaspberry pi, and that we can then send it elsewhere with MQTT so the data can be taken in, read, processed and displayed on our Node Red Dashboard. The code that we are using to make it work can be found on our Gitlab by following [this](https://gitlab.com/21s-itt2-datacenter-students-group/team-b3/-/blob/master/Code/v3/mqtt_sensor_21.py) link.

- Data Persistence via MongoDB coming soon(tm)
