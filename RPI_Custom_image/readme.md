# Custom RPI Images

## Description
IoT sensor systems are fragile when they are deployed and if anything happens to them you need to be able to quickly replace them at the customer site.

Having a RPi image with all your needed scripts (MQTT client etc.) and dependencies (python etc.) enables you to get a new sensor system up and running in short time.

1.  Make a list of needed scritps and installed dependencies needed on your sensor system.

## Raspberry OS Image

Basic updated Raspberry OS with be the base of which the system will be build apon.
We want to make sure the correct dependencies and scripts are installed, to make the system run correctly out of the box.


### Dependencies

Python - version 3.8
Pip

	Python3 get-pip.py

Paho-mqtt - version 1.5.1

	pip3 install paho-mqtt~=1.5.1

Digi.xbee.devices - version 1.4.0

	pip3 install digi-xbee~=1.4.0
	
### Scripts
We want to have a script that do most of the automations if there are settings that needs to be set and code that needs to be pulled.

	git clone somecode.on.our.gitlab.com

*!Some pathing should be included to makes sure its all in the correct folder*


## Links

[Exercise](https://eal-itt.gitlab.io/21s-itt2-project/exercises/exercises_ww18)
