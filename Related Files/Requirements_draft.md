Requirements Draft
=====================================

Technical Requirements  
------------

* DHT11 (Temp and Humidity sensor)
* TE Connectivity HPP801A031 (Temperature sensor)
* Raspberry Pi Pico
* Veroboard
* Cables/wires
* Resistors
* Power supply
* 9V battery connector
* NodeMCU
* Xbee s2c Modules
* Xbee Adaptor Board
