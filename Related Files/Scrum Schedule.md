|  **Date**  |  **Scrum Master**  |  **Scrum Secretary**  |
|  -----  |  -----  |  -----  |
| 24/02/21 | Aleksandra | Dainty |
| 01/03/21 | Lukasz | Aleksandra |
| 03/03/21 | Gladys | Dainty |
| 08/03/21 | Dainty | Gladys |
| 10/03/21 | TBD | TBD |



* **WHAT IS A SCRUM MASTER?**
The Scrum Master is the one who makes the agenda for the Daily Scrum and leads it. They keep the conversation flowing, set the time and focusing the incoherent ramblings of the team into useful strings of data that can be used for improving the product and the team. In this team, the facilitator role and the Scrum Master role are rolled into one.

* **WHAT IS A SCRUM SECRETARY**
The Scrum Secretary writes detailed notation of what was discussed during the Scrum so that it can be referred back to. They put their written notes in a mutually agreed upon location.
