**B3 has looked into how businesses work, and this is the information that we have gained. We have attempted to create an idea of what our business model might look like if the group decides to incorporate.**

- Our company works within 5 countries. Our business model is Business to Consumer, and our services are targeted towards the technically minded clients rather than big business itself. We make our money by selling our products and technical support for those products. 

- We work with making set user-specified product for our customers. This can range from large projects in the data centers to IOT systems to open and close a blind. 

- Our employee works as a think tank for the ideas and projects that our customers should now have in mind. Here we guide our customers to the product that suits them best  

- Our income is primarily from our product sales, but we offer a service agreement that our customers can purchase after the product is finished developed this is our second income  

**We decide to make our company a Private Limited Company for a couple of reasons.**
	
- 1. Tax Efficiency - Private Companies can write off more taxes than an entreprenurial company and have less people involved than a publicly traded company which for our purposes works great.
- 2. Partial Ownership - Continuing from the idea that there are less people involved in the process, partial ownership for us would mean that it's just us in charge for better or for worse. This gives us the flexibility we need to build this company in the way we want.
- 3. Economic Security - Companies CAN fail, but failure is less of a danger for us as an ApS. If we fail as an ApS we (The owners) cannot be sued for damages. As long as we are smart about how we keep our money invested we don't have to worry.
