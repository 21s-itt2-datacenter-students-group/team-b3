|  Measuring Technique  |  Sensor Name  |  Link to Supplier  |  Cost  |  Pros  |  Cons  |
| --- | --- | --- | --- | --- | --- |
| Temperature | TE Connectivity HPP801A031 | https://dk.rs-online.com/web/p/temperatursensor-og-fugtighedssensor-ic-er/8937082//?cm_mmc=aff2-_-dk-_-octopart-_-Instock8937082 |  60.52 dkk  |  Don't need to calibrate it to switch it out, Lead Free! | Kinda Pricey TBH |
|  Temp and Humidity  |  Seeed Studio Sensorudviklingssæt  |  https://dk.rs-online.com/web/p/sensor-udvikling/1743237/  |  49 dkk  |  It can compare temperature ***AND*** Humidity  |  It's already attached to a board ;_;  |
|  Humidity Sensor  |  HCZ-D5-A  |  https://dk.farnell.com/multicomp/hcz-d5-a/sensor-humidity-20-90-rh-5/dp/1891428  |  42,22 dkk  |  It covers the specifications that we need  |    |  
|  NONE  |  9v battery connector  |  https://dk.rs-online.com/web/p/batterikontakter/1854775/?cm_mmc=DK-PLA-DS3A-_-google-_-CSS_DK_DK_Batterier_og_opladere_Whoop-_-(DK:Whoop!)+Batterikontakter-_-1854775&matchtype=&pla-329509881343&gclid=Cj0KCQiAvvKBBhCXARIsACTePW8xjQNx2FKXu8LRzGhy5OnJBZNEfZciBEH01UmKfjvGuELeVEqD3UwaAoHREALw_wcB&gclsrc=aw.ds  |  6,09dkk  |  NONE  |  NONE  |
|  NONE  |  Stripboard  |  https://dk.rs-online.com/web/p/laboratorieprint-med-laengdestriber/0434627/  |  175,94dkk  |  NONE  |  NONE  |
