import paho.mqtt.client as mqtt
import time
import random

def on_message(client, userdata, message):
    print("message received " ,str(message.payload.decode("utf-8")))
    print("message topic=",message.topic)
    print("message qos=",message.qos)
    print("message retain flag=",message.retain)

'''broker_address="test.mosquitto.org"
print("creating new instance")
client = mqtt.Client("P1")
client.on_message=on_message
print("connecting to broker")
client.connect(broker_address)
client.loop_start()
print("Subscribing to topic","RNG")
client.subscribe("RNG")
on_message(client, userdata, message):'''

broker_address="test.mosquitto.org"
print("creating new instance")
client = mqtt.Client("P1") #create new instance
client.on_message=on_message #attach function to callback
print("connecting to broker")
client.connect(broker_address) #connect to broker
client.loop_start() #start the loop
print("Subscribing to RNG","Random Integer")
client.subscribe("RNG")
print("Publishing message to RNG",'Random Number')
client.publish("RNG",random.randint(1,10))
time.sleep(4) # wait
client.loop_stop() #stop the loop
