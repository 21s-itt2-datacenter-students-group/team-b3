# ROUGHEST OF DRAFTS (MK1, WIP, ETC)

* Environment is within operational limits (22c-35c) and (30-60% humidity)
* Price - Money is tight this project, so cheap parts are important
* Accuracy - would like to be at least 99% accurate
* Reliability - Hoping for 99% uptime
* Rarity - Being able to GET the parts we need is important
* Energy Consumption - We need to not break the bank with energy costs, and neither do we want to generate too much heat
* Size - This is the least important of the variables we came up with.
